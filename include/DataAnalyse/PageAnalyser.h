#ifndef PageAnalyser_h__
#define PageAnalyser_h__
#include <memory>
#include <string>

class DataProviderInterface;
class XMLSimilarityRateStrategyInterface;

class PageAnalyser
{
public:
	PageAnalyser(std::unique_ptr<DataProviderInterface> && originDataProvider, 
				 std::unique_ptr<DataProviderInterface> && diffDataProvider,
		std::unique_ptr<XMLSimilarityRateStrategyInterface> && rateStrategy,
		std::string originString);

	std::string analyze();

private:
	std::unique_ptr<DataProviderInterface> m_originDataProvider;
	std::unique_ptr<DataProviderInterface> m_diffDataProvider;
	std::unique_ptr<XMLSimilarityRateStrategyInterface> m_rateStrategy;
	std::string m_originString;
};
#endif // PageAnalyser_h__