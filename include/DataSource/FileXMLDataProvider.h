#ifndef FileXMLDataProvider_h__
#define FileXMLDataProvider_h__
#include "DataProviderIntreface.h"
#include <string>
#include <vector>

class FileXMLDataProvider : public DataProviderInterface
{
public:
	FileXMLDataProvider(std::string filename);
	~FileXMLDataProvider();

	virtual bool bad() override;
	virtual std::string badMessage() override;
	virtual const std::vector<std::shared_ptr<DataNode>> & getNodes() override;
	virtual std::shared_ptr<DataNode> getRootNode() const override;
	virtual std::string getNodeHeaderText(std::shared_ptr<DataNode> node) const override;
	virtual std::string getNodeFullText(std::shared_ptr<DataNode> node) const override;
	virtual std::string getNodePath(std::shared_ptr<DataNode> node) const override;

private:
	bool m_badFlag;
	std::string m_badMessage;

	class XMLImpl;

	XMLImpl* m_xmlImpl;

	std::shared_ptr<DataNode> m_rootNode;
	std::vector<std::shared_ptr<DataNode>> m_nodes;

};
#endif // FileXMLDataProvider_h__
