#ifndef DataProviderIntreface_h__
#define DataProviderIntreface_h__
#include <string>
#include <vector>

class DataNode;

class DataProviderInterface
{
public:
	virtual bool bad() = 0;
	virtual std::string badMessage() = 0;
	virtual const std::vector<std::shared_ptr<DataNode>> & getNodes() = 0;
	virtual std::shared_ptr<DataNode> getRootNode() const = 0;
	virtual std::string getNodeHeaderText(std::shared_ptr<DataNode> node) const = 0;
	virtual std::string getNodeFullText(std::shared_ptr<DataNode> node) const = 0;
	virtual std::string getNodePath(std::shared_ptr<DataNode> node) const = 0;


};
#endif // DataProviderIntreface_h__
