#ifndef XMLSimilarityRateStrategyFactory_h__
#define XMLSimilarityRateStrategyFactory_h__
#include <memory>
#include <RateStrategy/XMLSimilarityRateStrategyInterface.h>
#include <string>

/// \brief This class creates instances of algorithms (it does not follow the factory interface)
class XMLSimilarityRateStrategyStaticProvider
{
	// This is only a static factory for now
	XMLSimilarityRateStrategyStaticProvider();
public:
	static std::unique_ptr<XMLSimilarityRateStrategyInterface> createStrategyByName(const std::string & name);
};
#endif // XMLSimilarityRateStrategyFactory_h__
