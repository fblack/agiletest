#ifndef XMLSimilarityRateStrategyInterface_h__
#define XMLSimilarityRateStrategyInterface_h__
#include <memory>

class DataNode;
class DataProviderInterface;

class XMLSimilarityRateStrategyInterface
{

public:

	virtual float getRate(DataProviderInterface* originDataProvider, DataProviderInterface* diffDataProvider, std::shared_ptr<DataNode> originNode, std::shared_ptr<DataNode> node) const = 0;

};

#endif // XMLSimilarityRateStrategyInterface_h__