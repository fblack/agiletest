#ifndef SimpleDiffRateStrategy_h__
#define SimpleDiffRateStrategy_h__
#include <string>
#include "RateStrategy/XMLSimilarityRateStrategyInterface.h"

class SimpleDiffRateStrategy : public XMLSimilarityRateStrategyInterface
{
public:
	static std::string getStrategyNameStatic();
	virtual float getRate(DataProviderInterface* originDataProvider, DataProviderInterface* diffDataProvider, std::shared_ptr<DataNode> originNode, std::shared_ptr<DataNode> node) const override;
};


#endif // SimpleDiffRateStrategy_h__
