#include "RateStrategy/XMLSimilarityRateStrategyStaticProvider.h"
#include "SimpleDiffRateStrategy.h"

std::unique_ptr<XMLSimilarityRateStrategyInterface> XMLSimilarityRateStrategyStaticProvider::createStrategyByName(const std::string & name)
{
	if (name == SimpleDiffRateStrategy::getStrategyNameStatic()) {
		return std::move(std::make_unique<SimpleDiffRateStrategy>());
	}
}

