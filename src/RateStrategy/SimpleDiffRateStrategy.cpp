#include "SimpleDiffRateStrategy.h"
#include "DataSource/DataProviderIntreface.h"
#include <math.h>

static auto simpleDiffRateStrategyName = "simpledif";

std::string SimpleDiffRateStrategy::getStrategyNameStatic()
{
	return simpleDiffRateStrategyName;
}

float SimpleDiffRateStrategy::getRate(DataProviderInterface* originDataProvider, DataProviderInterface* diffDataProvider, std::shared_ptr<DataNode> originNode, std::shared_ptr<DataNode> node) const
{
	auto str1 = originDataProvider->getNodeHeaderText(originNode);
	auto str2 = diffDataProvider->getNodeHeaderText(node);
	int str1It = 0;

	// number of origin symbols left and new symbols
	int originQuantity = 0, newQuantity = 0, totalOriginQuantity = 0;
	// make a simple one-way string difference
	while (str1It != str1.size())
	{
		int str1ItNext = str1.find("\n", str1It);
		if (str2.find(str1.substr(str1It, str1ItNext - str1It))!=std::string::npos) {
			originQuantity++;
		} 
		else {
			newQuantity++;
		}
		str1It = str1ItNext + 1;
		totalOriginQuantity++;
	}
	if (originQuantity == 0) {
		return std::numeric_limits<float>::infinity();
	}

	float val = float (newQuantity + (totalOriginQuantity - originQuantity)) / originQuantity;
	return val;
}

