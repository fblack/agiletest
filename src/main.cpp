#include <string.h>
#include <string>
#include <unordered_map>
#include <iostream>
#include "DataAnalyse/PageAnalyser.h"
#include "RateStrategy/XMLSimilarityRateStrategyStaticProvider.h"
#include "DataSource/FileXMLDataProvider.h"
#include "RateStrategy/XMLSimilarityRateStrategyInterface.h"

static auto helpString =
"parameters of the app:\n"\
"origin=\"filepath\"			-	path to the origin file\n"\
"diff=\"filepath\"				-	path to the diff file\n"\
"algo={simpledif/visual/...}	-	comparing algorithm, default = simpledif\n"\
"refstring=str					-	reference string to look for\n"\
"##########################################################\n"\
"(c) Eugene Gavrilyuk for Agile Engine\n"\
"##########################################################\n";

static auto wrongParamString =
"Wrong input params... terminating\n";

static auto wrongAlgoString =
"Wrong algo name... terminating\n";

enum ReturnCodes
{
	rtSuccess = 0,
	rtWrongParam,
	rtWrongAlgo,
	rtWrongFile
};


int main(int argc, char *argv[])
{
	//////////////Parameters initialisation/////////////////

	if (argc == 1 || (argc == 2 && !strcmp(argv[0], "help"))) {
		std::cout << helpString;
		return rtSuccess;
	}

	// input params of the app
	std::unordered_map<std::string, std::string> params = 
	{	{"origin", ""},
		{"diff", ""},
		{"algo", ""},
		{"refstring", "id=\"make-everything-ok-button\""}
	};

	// parse params
	for (int pid = 1; pid < argc; ++pid)
	{
		// break input
		std::string curParamAndVal = argv[pid];
		std::string curParam, curVal;
		auto equalPos = curParamAndVal.find("=");

		if (equalPos == std::string::npos || equalPos == curParamAndVal.size()-1) {
			std::cout << wrongParamString;
			return rtWrongParam;
		}

		curParam = curParamAndVal.substr(0, equalPos);
		curVal = curParamAndVal.substr(equalPos + 1, curParamAndVal.size() - equalPos);

		if (curParam.empty() || curVal.empty() || params.count(curParam) == 0) {
			std::cout << wrongParamString;
			return rtWrongParam;
		}

		params[curParam] = curVal;
	}

	//////////////Init objects/////////////////

	std::unique_ptr<XMLSimilarityRateStrategyInterface> compareStrategy = XMLSimilarityRateStrategyStaticProvider::createStrategyByName(params["algo"]);
	std::unique_ptr<DataProviderInterface> originDataProvider = std::make_unique<FileXMLDataProvider>(params["origin"]);
	std::unique_ptr<DataProviderInterface> diffDataProvider = std::make_unique<FileXMLDataProvider>(params["diff"]);

	if (originDataProvider->bad()) {
		std::cout << "failed to load the origin file: " << originDataProvider->badMessage() << "\n";
		return rtWrongFile;
	}

	if (diffDataProvider->bad()) {
		std::cout << "failed to load the diff file: " << diffDataProvider->badMessage() << "\n";
		return rtWrongFile;
	}

	if (!compareStrategy) {
		std::cout << wrongAlgoString;
		return rtWrongAlgo;
	}

	PageAnalyser analyzer(std::move(originDataProvider), std::move(diffDataProvider), std::move(compareStrategy), params["refstring"]);
	std::string res = analyzer.analyze();

	if (res.empty()) {
		std::cout << "ref string not found\n";
	} else {
		std::cout << res << "\n";
	}
	return rtSuccess;
}