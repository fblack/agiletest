#include "DataAnalyse/PageAnalyser.h"
#include "RateStrategy/XMLSimilarityRateStrategyInterface.h"
#include "DataSource/DataProviderIntreface.h"
#include "DataSource/DataNode.h"
#include <stack>
#include <algorithm>

PageAnalyser::PageAnalyser(std::unique_ptr<DataProviderInterface> && originDataProvider,
	std::unique_ptr<DataProviderInterface> && diffDataProvider,
	std::unique_ptr<XMLSimilarityRateStrategyInterface> && rateStrategy,
	std::string originString) :
	m_originDataProvider(std::move(originDataProvider)),
	m_diffDataProvider(std::move(diffDataProvider)),
	m_rateStrategy(std::move(rateStrategy)),
	m_originString(originString)
{

}

std::string PageAnalyser::analyze()
{
	typedef std::pair<std::shared_ptr<DataNode>, float> NodeWeight;
	std::vector<NodeWeight> diffs;
	std::shared_ptr<DataNode> originNode;

	auto allOriginNodes = m_originDataProvider->getNodes();
	auto allDiffNodes = m_diffDataProvider->getNodes();
	
	// loop over the nodes to find the child
	for (auto node : allOriginNodes)
	{
		auto header = m_originDataProvider->getNodeHeaderText(node);

		if (header.find(m_originString) != std::string::npos) {
			originNode = node;
			break;
		}
	}

	// return nothing if found nothing
	if (!originNode) {
		return "";
	}

	// loop over the nodes to collect the diffs
	for (auto node : allDiffNodes)
	{
		auto header = m_diffDataProvider->getNodeHeaderText(node);

		diffs.push_back(std::make_pair(node, m_rateStrategy->getRate(m_originDataProvider.get(), m_diffDataProvider.get(), originNode, node)));
	}

	auto minNode = std::min_element(diffs.begin(), diffs.end(), [](const NodeWeight &a, const NodeWeight &b) {return a.second < b.second; });

	return m_diffDataProvider->getNodePath(minNode->first) + "\n\nNodeBody:\n"+ m_diffDataProvider->getNodeFullText(minNode->first);
}

