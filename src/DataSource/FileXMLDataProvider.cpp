#include "DataSource/FileXMLDataProvider.h"
#include <Utils/XML/pugixml.hpp>
#include <map>
#include <stack>
#include <sstream>
#include "DataSource/DataNode.h"

class FileXMLDataProvider::XMLImpl
{
public:
	pugi::xml_document doc;
	std::map<std::shared_ptr<DataNode>, pugi::xml_node> pugiMap;
	std::map<std::shared_ptr<DataNode>, std::vector<std::string>> pathMap;
};

FileXMLDataProvider::FileXMLDataProvider(std::string filename):
	m_badFlag(false)
{
	m_xmlImpl = new FileXMLDataProvider::XMLImpl();

	// load the document
	auto result = m_xmlImpl->doc.load_file(filename.c_str());

	// set bad flag in case of error
	if (result.status != pugi::status_ok) {
		m_badFlag = true;
		m_badMessage = result.description();
		return;
	}

	// traverse the nodes

	// stack of nodes with visited flag
	std::stack<std::pair<pugi::xml_node, bool>> nodes;

	m_rootNode = std::make_shared<DataNode>();

	nodes.push(std::make_pair(m_xmlImpl->doc.root(), false));

	// traverse through the nodes to find the child

	std::vector <std::string> path;

	while (!nodes.empty())
	{
		auto & curNode = nodes.top();
		if (curNode.second) {
			nodes.pop();
			path.pop_back();
			continue;
		}


		for (auto child : curNode.first.children()) {
			nodes.push(std::make_pair(child, false));
		}

		auto newNode = std::make_shared<DataNode>();
		m_xmlImpl->pugiMap.insert(std::make_pair(newNode, curNode.first));
		m_xmlImpl->pathMap.insert(std::make_pair(newNode, path));
		m_nodes.push_back(newNode);
		path.push_back(curNode.first.name());
		curNode.second = true;
	}
}

FileXMLDataProvider::~FileXMLDataProvider()
{
	delete m_xmlImpl;
}

bool FileXMLDataProvider::bad()
{
	return m_badFlag;
}

std::string FileXMLDataProvider::badMessage()
{
	return m_badMessage;
}

const std::vector<std::shared_ptr<DataNode>> & FileXMLDataProvider::getNodes()
{
	return m_nodes;
}

std::shared_ptr<DataNode> FileXMLDataProvider::getRootNode() const
{
	return m_rootNode;
}

std::string FileXMLDataProvider::getNodeHeaderText(std::shared_ptr<DataNode> node) const
{
	if (m_xmlImpl->pugiMap.count(node) == 0) {
		return "";
	}
	std::string ret;
	auto atrs = m_xmlImpl->pugiMap[node].attributes();
	for (auto it = atrs.begin(); it != atrs.end(); ++it)
		ret += std::string(it->name()) + "=\"" + it->as_string() + "\"\n";

	return ret;
}

std::string FileXMLDataProvider::getNodeFullText(std::shared_ptr<DataNode> node) const
{
	if (m_xmlImpl->pugiMap.count(node) == 0) {
		return "";
	}
	std::stringstream stream;

	m_xmlImpl->pugiMap[node].print(stream);

	return stream.str();
}

std::string FileXMLDataProvider::getNodePath(std::shared_ptr<DataNode> node) const
{
	if (m_xmlImpl->pathMap.count(node) == 0) {
		return "";
	}
	std::string ret = ">";
	for (auto it : m_xmlImpl->pathMap[node]) {
		ret += (it) + ">";
	}

	return ret;
}

